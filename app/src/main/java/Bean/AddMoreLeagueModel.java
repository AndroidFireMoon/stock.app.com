package Bean;

/**
 * Created by firemoon on 15/2/18.
 */

public class AddMoreLeagueModel {

    public String getLeagueName() {
        return LeagueName;
    }

    public void setLeagueName(String leagueName) {
        LeagueName = leagueName;
    }

    public String getEntryFees() {
        return EntryFees;
    }

    public void setEntryFees(String entryFees) {
        EntryFees = entryFees;
    }

    public String getWinningAmount() {
        return WinningAmount;
    }

    public void setWinningAmount(String winningAmount) {
        WinningAmount = winningAmount;
    }

    public String getNumberOfWinnern() {
        return NumberOfWinnern;
    }

    public void setNumberOfWinnern(String numberOfWinnern) {
        NumberOfWinnern = numberOfWinnern;
    }

    public String getNumberOfPlayers() {
        return NumberOfPlayers;
    }

    public void setNumberOfPlayers(String numberOfPlayers) {
        NumberOfPlayers = numberOfPlayers;
    }

    public String getComments() {
        return Comments;
    }

    public void setComments(String comments) {
        Comments = comments;
    }

    private String LeagueName;
    private String EntryFees;
    private String WinningAmount;
    private String NumberOfWinnern;
    private String NumberOfPlayers;
    private String Comments;

}
