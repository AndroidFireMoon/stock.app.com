package Bean;

/**
 * Created by firemoon on 13/2/18.
 */

public class DataSet {


    public String getLeagueName() {
        return LeagueName;
    }

    public void setLeagueName(String leagueName) {
        LeagueName = leagueName;
    }

    public String getPortfolioid() {
        return Portfolioid;
    }

    public void setPortfolioid(String portfolioid) {
        Portfolioid = portfolioid;
    }

    public String getLeagueID() {
        return LeagueID;
    }

    public void setLeagueID(String leagueID) {
        LeagueID = leagueID;
    }

    public String getUserID() {
        return UserID;
    }

    public void setUserID(String userID) {
        UserID = userID;
    }

    public String getAmount() {
        return Amount;
    }

    public void setAmount(String amount) {
        Amount = amount;
    }

    public String getPlayedDate() {
        return PlayedDate;
    }

    public void setPlayedDate(String playedDate) {
        PlayedDate = playedDate;
    }

    public String getRank() {
        return Rank;
    }

    public void setRank(String rank) {
        Rank = rank;
    }

    public String getShareType() {
        return ShareType;
    }

    public void setShareType(String shareType) {
        ShareType = shareType;
    }

    public String getKingName() {
        return KingName;
    }

    public void setKingName(String kingName) {
        KingName = kingName;
    }

    public String getQueenName() {
        return QueenName;
    }

    public void setQueenName(String queenName) {
        QueenName = queenName;
    }

    private String LeagueName;
    private String Portfolioid;
    private String LeagueID;
    private String UserID;
    private String Amount;
    private String PlayedDate;
    private String Rank;
    private String ShareType;
    private String KingName;
    private String QueenName;

}
