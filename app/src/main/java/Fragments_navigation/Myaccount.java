package Fragments_navigation;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.game.stock.stockapp.R;


public class Myaccount extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //returning our layout file
        //change R.layout.yourlayoutfilename for each of your fragments
        /*Toolbar toolbar = (Toolbar) container.findViewById(R.id.toolbar_myleague);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
//        ((AppCompatActivity) getActivity()).getSupportActionBar().hide();
        //toolbar.setNavigationIcon(R.drawable.ic_toolbar);
        toolbar.setTitle("hidg");
        toolbar.setSubtitle("hfis");*/
        return inflater.inflate(R.layout.fragment_myaccount, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("MY ACCOUNT");

    }
}
