package Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.game.stock.stockapp.R;

import java.util.List;

import Bean.DataSet;
import Util.Controller;

/**
 * Created by firemoon on 13/2/18.
 */

public class LeagueListAdapter extends BaseAdapter {

    private Activity activity;
    private LayoutInflater inflater;
    private List<DataSet> DataList;
    private TextView leaugeName,amount,kingName,queenName;
//    ImageLoader imageLoader = Controller.getPermission().getImageLoader();

    public LeagueListAdapter(Activity activity, List<DataSet> dataitem) {
        this.activity = activity;
        this.DataList = dataitem;
    }

    @Override
    public int getCount() {
        return DataList.size();
    }

    @Override
    public Object getItem(int position) {
        return DataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null)
            convertView = inflater.inflate(R.layout.my_leaguelist, null);

       /* if (imageLoader == null)
            imageLoader = Controller.getPermission().getImageLoader();*/
        NetworkImageView thumbNail = (NetworkImageView) convertView.findViewById(R.id.thumbnail);

//        getUiId(convertView);
        leaugeName = (TextView) convertView.findViewById(R.id.txt_LeagueName);
        amount = (TextView) convertView.findViewById(R.id.txt_Amount);
        kingName = (TextView) convertView.findViewById(R.id.txt_KingName);
        queenName = (TextView) convertView.findViewById(R.id.txt_QueenName);

        DataSet m = DataList.get(position);
        leaugeName.setText(m.getLeagueName());
        amount.setText(m.getAmount());
        kingName.setText(m.getKingName());
        queenName.setText(m.getQueenName());

        return convertView;
    }

    public void getUiId(View convertView)
    {

    }
}
