package Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;
import com.game.stock.stockapp.R;

import java.util.List;

import Bean.AddMoreLeagueModel;
import Bean.AddMoreLeagueModel;

/**
 * Created by firemoon on 15/2/18.
 */

public class AddMoreLeagueAdapter extends BaseAdapter {

    private Activity activity;
    private LayoutInflater inflater;
    private List<AddMoreLeagueModel> DataList;
    private TextView leaugeName,entryFees,totalWinning,winners,numberOfTeams;


    public AddMoreLeagueAdapter(Activity activity, List<AddMoreLeagueModel> dataitem) {
        this.activity = activity;
        this.DataList = dataitem;
    }

    @Override
    public int getCount() {
        return DataList.size();
    }

    @Override
    public Object getItem(int position) {
        return DataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null)
            convertView = inflater.inflate(R.layout.add_myleague, null);

       /* if (imageLoader == null)
            imageLoader = Controller.getPermission().getImageLoader();*/
        NetworkImageView thumbNail = (NetworkImageView) convertView.findViewById(R.id.thumbnail);

//        getUiId(convertView);
        leaugeName = (TextView) convertView.findViewById(R.id.txt_LeagueName);
        entryFees = (TextView) convertView.findViewById(R.id.txt_EntryFees);
        totalWinning = (TextView) convertView.findViewById(R.id.txt_totalWinnings);
        winners = (TextView) convertView.findViewById(R.id.txt_Winners);
        numberOfTeams = (TextView) convertView.findViewById(R.id.txt_numberteams);

        AddMoreLeagueModel addMoreLeagueModel = DataList.get(position);
        leaugeName.setText(addMoreLeagueModel.getLeagueName());
        entryFees.setText(addMoreLeagueModel.getEntryFees());
//        kingName.setText(addMoreLeagueModel.getKingName());
//        queenName.setText(addMoreLeagueModel.getQueenName());

        return convertView;
    }

    public void getUiId(View convertView)
    {

    }
}

