package com.game.stock.stockapp;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by firemoon on 12/1/18.
 */

public class Config {

    /// SHAREDPREFRENCES VALUES



// BASE URL

    private static String BASEURL= "http://www.bstarsw.in/royalbull71/web2/user.php?act=" ;
    private static String BASE= "http://www.bstarsw.in/royalbull71/web2/" ;
    private static String BASEVIEW= "http://www.bstarsw.in/royalbull71/web2/myleagues.php?view=" ;


    ///   APPLICATION URL

//    public static String registerUrl="http://bstarsw.in/Royalbull/web/Registration.php";
    public static String registerUrl=BASEURL+"add";

    public static String loginUrl= BASE+"log.php";
    public static String leaguelist= BASEVIEW+"all";
    public static String verifyPAN= BASEURL+"updatep";
    public static String verifyBANK= BASEURL+"updateb";
    public static String updateProfile= BASEURL+"update";
    public static String alldetailUser= "http://www.bstarsw.in/royalbull71/web2/user.php?view=single";
    public static String changePassword= BASEURL+"changep";

    //for login
    public static final String emaillogin = "email";
    public static final String passwordlogin = "password";
    //for register
    public static final String userid = "userid";
    public static final String username = "username";
    public static final String email = "email";
    public static final String password = "password";

    public static final String phoneno = "mobile";


    //LOGIN PARAMETER

    public static final String LOGIN_FULL_NAME = "Full Name";
    public static final String LOGIN_EMAILID = "EmailId";
    public static final String LOGIN_MOBILE = "MobileNo";
    public static final String LOGIN_CITY = "City";
    public static final String LOGIN_BLOCK = "Block";
    public static final String LOGIN_IS_WITHDRAW = "IsWithdrawal";
    public static final String LOGIN_IS_MOBILE_VERIFY = "IsVerifyMobile";
    public static final String LOGIN_IS_STATE_VERIFY = "IsverifyState";
    public static final String LOGIN_IS_ACCOUNT_VERIFY = "IsverifyAccount";
    public static final String LOGIN_IS_PROOF_VERIFY = "IsVerifyProof";
    public static final String LOGIN_PAN_NAME = "PANName";
    public static final String LOGIN_PAN_NUMBER  = "PANNumber";
    public static final String LOGIN_DOB  = "DOB";
    public static final String LOGIN_BANKSTATE  = "BankState";
    public static final String LOGIN_IMAGE  = "Image";
    public static final String LOGIN_NAME_ON_ACCOUNT  = "NameOnAccount";
    public static final String LOGIN_BANK_ACCOUNT  = "BankAccountNO";
    public static final String LOGIN_BIFSC  = "BIFSC";
    public static final String LOGIN_BALANCE = "Balance";
}
