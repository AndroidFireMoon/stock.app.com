package com.game.stock.stockapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import Util.AppUrl;
import Util.DialogInterfacecustom;
import Util.Validation;
import web.CheckInternetConnectio;
import web.IResponse;
import web.Web;

public class Register extends AppCompatActivity implements View.OnClickListener{
    Button login, register,btn_verify;
    EditText username, fullname, email, password, phone,edttxt_enterOTP;
    private AwesomeValidation awesomeValidation;

    ProgressDialog mProgressDialog;
    private android.os.Handler handler;
    // Create string variable to hold the EditText Value.
    String FirstName,EmailAddress,Password ,MobileNo;
    private RequestQueue requestQueue;
    private LinearLayout linearLayout_footer,linearLayout_login,linearLayout_otpVerification;

    private TextView txt_resendOtp;

    // Mobile Number OTP Verification
    private FirebaseAuth mAuth;
    private PhoneAuthProvider.ForceResendingToken mResendToken;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;
    String mVerificationId;

    String MobilePattern = "[0-9]{10}";
    private static final String TAG = "PhoneAuthActivity";
    private TextView otp_phoneNumber;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        setContentView(R.layout.activity_register);

         toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.bakbtnresize);
        toolbar.setTitle("Registration");
        setSupportActionBar(toolbar);


        //initialise values
        awesomeValidation = new AwesomeValidation(ValidationStyle.BASIC);
        username = (EditText) findViewById(R.id.username);
        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);
        phone = (EditText) findViewById(R.id.phone);
        edttxt_enterOTP = (EditText) findViewById(R.id.edttxt_enterOTP);
        otp_phoneNumber = (TextView) findViewById(R.id.otp_phoneNumber);

        requestQueue = Volley.newRequestQueue(Register.this);

// Assigning Activity this to progress dialog.
        mProgressDialog = new ProgressDialog(Register.this);
        login = (Button) findViewById(R.id.login);
        register = (Button) findViewById(R.id.register);
        btn_verify = (Button) findViewById(R.id.btn_verify);
        linearLayout_login = (LinearLayout) findViewById(R.id.linearlayout_login);
        linearLayout_footer = (LinearLayout) findViewById(R.id.footer);
        linearLayout_otpVerification = (LinearLayout) findViewById(R.id.linearlayout_otpVerification);
        txt_resendOtp = (TextView) findViewById(R.id.txt_resendOtp);

        btn_verify.setOnClickListener(this);
        txt_resendOtp.setOnClickListener(this);


        getSupportActionBar().setDisplayShowTitleEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Register.this, Login.class));
            }
        });
        register.setOnClickListener(this);

        firebaseMobileVerification();
    }

    public void firebaseMobileVerification()
    {
        mAuth = FirebaseAuth.getInstance();

        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onVerificationCompleted(PhoneAuthCredential credential) {
                Log.d(TAG, "onVerificationCompleted:" + credential);
                signInWithPhoneAuthCredential(credential);
            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                Log.w(TAG, "onVerificationFailed", e);
                if (e instanceof FirebaseAuthInvalidCredentialsException) {
                    phone.setError("Invalid phone number.");
                } else if (e instanceof FirebaseTooManyRequestsException) {
                    Snackbar.make(findViewById(android.R.id.content), "Quota exceeded.",
                            Snackbar.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCodeSent(String verificationId,
                                   PhoneAuthProvider.ForceResendingToken token) {
                Log.d(TAG, "onCodeSent:" + verificationId);
                mVerificationId = verificationId;
                mResendToken = token;
            }
        };
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = task.getResult().getUser();

                            register();
//                            startActivity(new Intent(Register.this, Login.class));
//                            finish();
                        } else {
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {

                                // Hiding the progress dialog after all task complete.
                                mProgressDialog.cancel();
                                mProgressDialog.dismiss();
                                edttxt_enterOTP.setError("Invalid code.");
                            }
                        }
                    }
                });
    }

    private void startPhoneNumberVerification(String phoneNumber) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                mCallbacks);        // OnVerificationStateChangedCallbacks
    }

    private void verifyPhoneNumberWithCode(String verificationId, String code) {
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, code);
        signInWithPhoneAuthCredential(credential);
//        register();

    }

    private void resendVerificationCode(String phoneNumber,PhoneAuthProvider.ForceResendingToken token) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                mCallbacks,         // OnVerificationStateChangedCallbacks
                token);             // ForceResendingToken from callbacks
    }
    private boolean validatePhoneNumber() {
        String phoneNumber = MobileNo;
        if (TextUtils.isEmpty(phoneNumber)) {
            phone.setError("Invalid phone number.");
            return false;
        }
        return true;
    }

    @Override
    public void onStart() {
        super.onStart();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if (currentUser != null) {
//            startActivity(new Intent(Register.this, MainActivity.class));
//            finish();
        }
    }

    public void CheckEditTextIsEmptyOrNot() {


        // Getting values from EditText.
        FirstName = username.getText().toString().trim();
        EmailAddress = email.getText().toString().trim();
        MobileNo= "+91"+phone.getText().toString().trim();
        Password= password.getText().toString().trim();

        CheckInternetConnectio _checkInternetConnection = new CheckInternetConnectio(Register.this);

        if (_checkInternetConnection.checkInterntConnection()) {

            if (TextUtils.isEmpty(username.getText().toString()) || TextUtils.isEmpty(email.getText().toString()) || TextUtils.isEmpty(password.getText().toString())) {
                Toast.makeText(Register.this, "All Field are required", Toast.LENGTH_SHORT).show();
            } else {
                if (Validation.isEmailAddress(email, true)) {
                    String Password = password.getText().toString();
                    if (Password.equalsIgnoreCase("")) {

                        Toast.makeText(Register.this, "Enter Password", Toast.LENGTH_LONG).show();

                    } else {

                        if(phone.getText().toString().matches(MobilePattern)) {

//                            Toast.makeText(getApplicationContext(), "phone number is valid", Toast.LENGTH_SHORT).show();

                            Toast.makeText(Register.this, "Please Verify Phone Number", Toast.LENGTH_LONG).show();
                            mProgressDialog = ProgressDialog.show(Register.this, null,
                                    "Please Wait....", true);
                            mProgressDialog.setCancelable(true);
                            mProgressDialog.dismiss();

                            linearLayout_login.setVisibility(View.GONE);
                            linearLayout_footer.setVisibility(View.GONE);
                            linearLayout_otpVerification.setVisibility(View.VISIBLE);


                            toolbar.setTitle("OTP Verification");
                            setSupportActionBar(toolbar);

                            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    linearLayout_login.setVisibility(View.VISIBLE);
                                    linearLayout_footer.setVisibility(View.VISIBLE);
                                    linearLayout_otpVerification.setVisibility(View.GONE);
                                    toolbar.setTitle("Registration");
                                    setSupportActionBar(toolbar);
                                    toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            finish();
                                }
                            });
                                }
                            });
                            otp_phoneNumber.setText(MobileNo);
                            startPhoneNumberVerification(MobileNo);

                        }
                        else {
                            Toast.makeText(Register.this, "Enter Phone Number Correct", Toast.LENGTH_LONG).show();
                        }
                    }
                } else {
//                    phone.setText("");

                    email.setText("");
                    Toast.makeText(Register.this, "Enter valid Email Id",
                            Toast.LENGTH_SHORT).show();
                }


            }


        } else {
            Toast.makeText(Register.this, "Check Internet Connection",
                    Toast.LENGTH_SHORT).show();

        }
    }
    private void register() {

        // Showing progress dialog at user registration time.


        final String otp_phoneNumberq = otp_phoneNumber.getText().toString().trim();
        // Creating string request with post method.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.registerUrl, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String ServerResponse) {

                        // Hiding the progress dialog after all task complete.

                        try {
                            JSONObject jsonObject=new JSONObject(ServerResponse);

                                if(jsonObject.getString("status").equalsIgnoreCase(String.valueOf(1))){


                                    SharedPreferences sharedPreferences_customLogin=getSharedPreferences("com.custom.Login", Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editor=sharedPreferences_customLogin.edit();
//                                    editor.putString("id","userid");
//                                    editor.putString("mobile",MobileNo);
                                    editor.putString("password",password.getText().toString());
                                    editor.commit();
                                    // If response matched then show the toast.
                                    Toast.makeText(Register.this, ""+jsonObject.getString("msg").toString(), Toast.LENGTH_LONG).show();

                                    Intent intent = new Intent(Register.this, Login.class);

                                    // Sending User Email to another activity using intent.
                                    //intent.putExtra(Config.fullname, fullname1);

                                    startActivity(intent);
                                // Finish the current Login activity.
                                finish();



                            }else
                            {
                                mProgressDialog.cancel();
                                mProgressDialog.dismiss();
                                Toast.makeText(getApplicationContext(),""+jsonObject.getString("msg").toString(),Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {

                            mProgressDialog.cancel();
                            mProgressDialog.dismiss();
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                        // Hiding the progress dialog after all task complete.
                        mProgressDialog.dismiss();

                        // Showing error message if something goes wrong.
                        Toast.makeText(Register.this, volleyError.toString(), Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {

                // Creating Map String Params.
                Map<String, String> params = new HashMap<String, String>();

                // Adding All values to Params.
                // The firs argument should be same sa your MySQL database table columns.
               // params.put("User_Email", EmailHolder);
               // params.put("User_Password", PasswordHolder);
                params.put(Config.username, FirstName);
                params.put(Config.email,EmailAddress);
                params.put(Config.password, Password);
                params.put(Config.phoneno, otp_phoneNumberq);
//                params.put(Config.phoneno, MobileNo);

                return params;
            }

        };

        // Creating RequestQueue.
        RequestQueue requestQueue = Volley.newRequestQueue(Register.this);

        // Adding the StringRequest object into requestQueue.
        requestQueue.add(stringRequest);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.register:
                CheckEditTextIsEmptyOrNot();
                if (validatePhoneNumber()) {

                }

                break;
            case R.id.btn_verify:
                mProgressDialog.setMessage("Please Wait");
                mProgressDialog.show();
                String code = edttxt_enterOTP.getText().toString();
                if (TextUtils.isEmpty(code)) {
                    edttxt_enterOTP.setError("Cannot be empty.");
                    return;
                }

                verifyPhoneNumberWithCode(mVerificationId, code);
                break;
            case R.id.txt_resendOtp:
                resendVerificationCode(MobileNo, mResendToken);
                Toast.makeText(getApplicationContext(),"OTP is sent again please wait",Toast.LENGTH_SHORT).show();
                break;
        }

    }

}

/*

        register.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                CheckInternetConnectio _checkInternetConnection = new CheckInternetConnectio(
                        Register.this);

                if (_checkInternetConnection.checkInterntConnection()) {

                    if (TextUtils.isEmpty(fullname.getText().toString()) || TextUtils.isEmpty(email.getText().toString()) || TextUtils.isEmpty(password.getText().toString()) || TextUtils.isEmpty(phone.getText())) {
                        Toast.makeText(Register.this, "All Field are required", Toast.LENGTH_SHORT).show();

                    } else {

//
                      if (Validation.isEmailAddress(email, true)) {
                            if (Validation.isValidPassword(password.getText().toString())) {

                                if (Validation.isMobileNoValid(phone.getText().toString())){

                                    register();
                                    mProgressDialog = ProgressDialog.show(Register.this, null,
                                            "Please Wait....", true);
                                    mProgressDialog.setCancelable(true);


                                }

                            } else {
//                                    email.setError("Enter valid Email Id");
                                email.setText("");
                                Toast.makeText(Register.this, "Enter valid Email Id",
                                        Toast.LENGTH_SHORT).show();

                            }}else {
                                phone.setText("");
                                Toast.makeText(Register.this, "Enter valid Mobile No. & it should starts with 9",
                                        Toast.LENGTH_LONG).show();

                            }


                        }



                } else {
                    Toast.makeText(Register.this, "Check Internet Connection",
                            Toast.LENGTH_SHORT).show();

                }


            }


        });


    }


    private void register() {

        new Thread(new Runnable() {
            @Override
            public void run() {
                // TODO Auto-generated method stub
                List<NameValuePair> data=new ArrayList<NameValuePair>();
                data.add(new BasicNameValuePair("fullname",fullname.getText().toString()));

                data.add(new BasicNameValuePair("email", email.getText().toString()));
                data.add(new BasicNameValuePair("password",password.getText().toString()));

                data.add(new BasicNameValuePair("phone", phone.getText().toString()));


                new Web().requestPostStringData(AppUrl.registerUrl, data, Register.this, 100);



            }
        }).start();

    }


    @Override
    public void onComplete(final String result, int i) {
        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        mProgressDialog.cancel();

        handler.post(new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                JSONObject obj = null;
                try {
                    obj = new JSONObject(result);
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                try {
                    if (obj.getString("error").equals("false")) {
                       /* Intent intent = new Intent(SignIn.this, BaseActivity.class);
                        startActivity(intent);
                   */
                        /*startActivity(new Intent(Register.this,Login.class));


                        Toast.makeText(Register.this, "Successfully Login....", Toast.LENGTH_LONG).show();


                        JSONObject jObj2=obj.getJSONObject("data");
                        SharedPreferences sharedpreferences = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedpreferences.edit();
                        editor.putBoolean("LOGIN", true);
                        editor.putString("USER_ID", jObj2.getString("user_id"));
                      /*  editor.putString("MOBILE", jObj2.getString("mobile"));
                        editor.putString("EMAIL", jObj2.getString("email"));
                        editor.putString("NAME", jObj2.getString("username"));
                        editor.putString("USER_AUTH", jObj2.getString("auth_token"));
*/
              /*          editor.commit();

                        finish();


                    } else {
                        DialogInterfacecustom.loginResponceDialog(Register.this, obj.getString("message").toString(), "");


                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        });



    }

}
*/