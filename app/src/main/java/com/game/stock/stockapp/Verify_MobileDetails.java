package com.game.stock.stockapp;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Calendar;


public class Verify_MobileDetails extends Fragment {

    private TextView txt_PAN,dateView,txt_mobNumber;
    private EditText edt_PAN,edt_name,edt_mob;

    private Button btn_submitverification;
    LinearLayout linear_MOBILE;
    // DATE PICKER
    DatePickerDialog.OnDateSetListener date;
    private Calendar myCalendar;
    private ProgressDialog mProgressDialog;
    private String id,name;
    private SharedPreferences sharedPreferences_customLogin;
    private String pannum,datedob;
    private String mobile_num;

    public Verify_MobileDetails() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.verifyaccount_mob, container, false);

        edt_mob = (EditText) view.findViewById(R.id.edt_mob);
        txt_mobNumber = (TextView) view.findViewById(R.id.txt_mobNumber);

        sharedPreferences_customLogin = getActivity().getSharedPreferences("com.custom.Login", Context.MODE_PRIVATE);
        mobile_num = sharedPreferences_customLogin.getString("mobile", null);
        if(mobile_num!="")

        {
            txt_mobNumber.setText(mobile_num);
        }
        return view;
    }

}