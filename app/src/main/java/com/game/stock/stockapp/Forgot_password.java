package com.game.stock.stockapp;

import android.content.Intent;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Forgot_password extends AppCompatActivity {

    Button login;
    EditText login_emailid;
   // private AwesomeValidation awesomeValidation;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        login=(Button) findViewById(R.id.login);
        login_emailid=(EditText) findViewById(R.id.login_emailid);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.bakbtnresize);
        toolbar.setTitle("Forgot Password");
        setSupportActionBar(toolbar);



        getSupportActionBar().setDisplayShowTitleEnabled(true);


        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),Login.class));
            }
        });

        login.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {

                final String email = login_emailid.getText().toString();
                if (!isValidEmail(email)) {
                    login_emailid.setError("Invalid Email");
                }

                // Start NewActivity.class
                Intent myIntent = new Intent(Forgot_password.this,
                        Login.class);
                startActivity(myIntent);
            }
        });





        }

    private boolean isValidEmail(String email) {


        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

}
